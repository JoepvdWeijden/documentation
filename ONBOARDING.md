## Onboarding

In the picture below we describe the four step process of onboarding to the SURF Streaming Data Platform for [The Green Village](https://www.thegreenvillage.org/). 

In step 2 of the process we present two documents: the Service Level Agreement with SURF and the Verwerkersovereenkomst of SURF. TODO insert links to the documents.

In step 4 we talk about the preperation of the platform for your sensors and data coming from the sensors. More detailed information about this can be found [here](preparation_platform.md). 

![The Green Village Data Platform at a glance](images/Onboarding-process-diagram.png)