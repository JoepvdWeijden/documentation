# Platform overview
## Who should read this page?
This document provides an overview of The Green Village Data Platform for cluster administrators, project users and data stewards.

Access to the platform and its data can be arranged via [The Green Village](https://www.thegreenvillage.org/). If you are interested in integrating your project with the platform, please see the [onboarding documentation](ONBOARDING.md).

## The Green Village Data Platform
[The Green Village](https://www.thegreenvillage.org/) is a place to process, store, visualize and share sensor data. 

- It is a **scalable data streaming platform**. Real-time sensor data can be streamed into the platform, and consumed by any number of applications in real-time.

- It is a **collaboration platform**. Every project taking part in The Green Village receives individual resources in the platform. Devices and measurements can be found through the research data management component, and shared with other projects through an administration interface.

- It is a **data storage platform**. Messages in the platform's data streams are automatically aggregated into several data stores. Standardised data formats allow for quick exchange of data, and for automatic extraction and conversion of sensor and measurement data. Metadata search among all projects is enabled through the research data management component.

- It is a **monitoring and business intelligence platform**. New sensors and measurements are automatically exposed in projects' dashboards, enabling rapid data exploration. Projects can design custom dashboards and expose them to other projects or make them public.
Monitoring and alerting is in-place on a project and cluster level, with dashboards showing metrics on the health and performance of the platform's components. Pre-defined alerts notify cluster administrators of any problems.

- It is an **open-source platform**, built fully out of open-source components. The platform is cloud-agnostic and can be installed and run on any cloud provider. **TODO: put open source licencing in place**.
The Green Village Data Platform is hosted on SURFsara premises. The core services of the platform are deployed in a highly-available and fault tolerant manner.

- It is a **secure platform**. Authentication and authorization are in place to ensure separation of projects. Furthermore, all access end-points of the platform are secured. The platform complies with all security standards of SURFsara, including the ISO/IEC 27001 standard.

## Core components
The core components of The Green Village Data Platform are depicted in the diagram below (in color):

![Core components of The Green Village Data Platform](images/core-services.png)

There are three core platform components, together providing common services used by all projects:
* A [Kafka](https://kafka.apache.org/) cluster: the streaming backbone service.
* A central [Grafana](https://grafana.com) dashboarding service used by the cluster administrator to monitor the health and performance of the platform.
* A research data management (RDM) service that defines common data formats and provides a central database for metadata from all projects.

In addition, there is a project management and data sharing API used by the cluster administrator.

For installing the core components of the platform on Kubernetes, see the [cluster installation documentation](CLUSTER-INSTALLATION.md).

## Project-specific components
In addition to the platform's core components, each project will use a number of isolated, project-specific components. These project-specific components of The Green Village Data Platform are depicted in the diagram below (in color):

![Project resources in The Green Village Data Platform](images/project-resources.png)

Individual projects make use of the following private resources.
* Every project gets an isolated data stream (topic) in Kafka.
* Every project has its own [Minio](https://www.minio.io) object data store for historical data.
* Every project has its own [InfluxDB](https://www.influxdata.com) time series database for visualisation.
* Every project has its own automated data pipeline to re-format and replicate data into the storage components and extract metadata.
* Lastly, each project will be able to explore devices and quantities dynamically through the dashboards exposed in each project's [Grafana](https://grafana.com) dashboarding instance.

Project dashboards will also expose and alert on metrics for monitoring the project's resource usage, such as message rate or disk usage. New dashboards can be created and shared with other projects, and additional alerts can be defined.

For installing projects on the platform and for a detailed overview of the project-specific components, see the [project installation documentation](PROJECT-INSTALLATION.md).

### Automated data pipeline
Messages can be sent to the platfrom using a Kafka client or HTTP endpoint. All messages will be stored in Kafka **for a period of 7 days** and therefore do not have to be read immediately. Messages are automatically saved to the object store for long-term storage, and processed using the [automated data pipeline](DATA.md#automated-data-pipeline).

A predefined generic schema ensures that all messages will be in a compatible data format. This schema consists of two parts: 1.) a *data part* that holds the timestamp of a measurement and an array of individual sensor readings, variable names and units, and 2.) a *metadata part* that holds the information of the device that produced the measurement, such as the device type and location.

An overview of the components of the automated data pipeline is provided in the diagram below:

![Automated data platform](images/automated-data-pipeline.png)

The automated data pipeline transforms and saves data into a number of formats that allow for quick analysis and visualisation:
* Raw message data in Kafka is saved into the Minio object store
* Data in Kafka is saved to the InfluxDB time series database
* Raw data in Minio is deduplicated and converted into JSON files per day and per device
* Sensor and measurement metadata is extracted and stored in the central metadata database and in files in the Minio object store
* Easy-to-read CSV files with measurement data are created, including metadata

More information about the automated data pipeline can be found in the [relevant documentation](DATA.md#automated-data-pipeline).

## Data stores and formats
The platform allows for working with the data in different ways by saving it into different data stores with different formats. A detailed description of the different data stores and formats can be found in the [data documentation](DATA.md). For a brief summary, please see the table below:

| **Data store** | **Data format** | **Purpose** |
| :---- | :---- | :---- |
| Kafka | Avro | Real-time data consumption |
| InfluxDB | Time series | Visualisation and time series queries |
| Minio | Avro | Analysis of raw sensor data |
| Minio | JSON - data | Programmatic analysis of sensor data |
| Minio | CSV | Manual analysis of sensor data and metadata (e.g. with a spreadsheet editor) |
| PostgreSQL | | Central metadata catalogue |

## Research data management
The research data management (RDM) component stores metadata for all sensors and quantities across all projects. The RDM component can answer queries like:

* Which devices belong to project X?
* List all the files for device Y.
* Which devices measure quantity Z?
* Which quantities are measured in project X?
* How many messages did device Y measure on a particular day?

New projects are automatically and directly integrated into the metadata database. This means projects can search for devices and quantities of interest and receive an answer on what is available in the platform in real-time.

## Sharing data
After identifying interesting devices and quantities of another project, that project's coordinator can be contacted to obtain access to the data. Different data sets can be shared between projects:

* Sharing data streams from Kafka
* Sharing time series databases from InfluxDB
* Sharing raw sensor data from Minio
* Sharing JSON sensor data from Minio
* Sharing CSV data from Minio

Lastly, individual time series can be made public and exposed to the 3D visualisation interface. Detailed information about sharing data can be found in the [sharing documentation](SHARING.md).

### Complete picture/overview and links
We will end this section with a figure depicting the complete picture of the platform:

![The complete picture of the Green Village Data Platform](images/full-picture.png)

More information about the platform can found at the following links:

* [Documentation](https://gitlab.com/the-green-village/documentation)
* [Example code](https://gitlab.com/the-green-village/examples)
* [Project code](https://gitlab.com/the-green-village/projects)
