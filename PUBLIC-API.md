# 3. Public API
The public HTTP API exposes publicly readable time series from InfluxDB to the outside world. To expose time series, use the cluster management API as described in the [public sharing document](sharing/SHARING-PUBLIC.md).

The public HTTP API exposes two paths: one for listing public time series by project ID, and another for getting public time series data by the time series' quantity UID and device UID. Only data for the last seven days is exposed, aggregated by mean in one-hour intervals.

## Listing public time series
The path for listing public time series is located at [https://openfaas.green-village.sda-projects.nl/function/tgv-3d-list-public-series](https://openfaas.green-village.sda-projects.nl/function/tgv-3d-list-public-series).

To list public time series for project A, call the API as follows:

```sh
$ curl -X GET \
  "https://openfaas.green-village.sda-projects.nl/function/tgv-3d-list-public-series?project_id=A" \
  -H "accept: application/json" | jq
[
    {
        "device_uid": 1,
        "metadata": {
            "description": "Heat index",
            "name": "heat_index_c",
            "type": "FLOAT",
            "unit": "°C"
        },
        "quantity_uid": 2
    }
    ...
]
```

As you can see, project A exposes one public time series, identified by device UID `1` and quantity UID `2`.

## Getting public time series data
To get the last seven days' worth of data for the time series identified by device UID `1` and quantiy UID `2`, call the relevant path at [https://openfaas.green-village.sda-projects.nl/function/tgv-3d-get-public-series](https://openfaas.green-village.sda-projects.nl/function/tgv-3d-get-public-series) with these UIDs as query parameters. The returned data points are aggregated on an hourly basis.

```sh
curl -X GET \
  "https://openfaas.green-village.sda-projects.nl/function/tgv-3d-get-public-series?device_uid=1&quantity_uid=2" \
  -H "accept: application/json" | jq
{
    "metadata": {
        "quantity": {
            "description": "Temperature",
            "name": "temp_c",
            "type": "FLOAT",
            "unit": "°C"
        }
    },
    "points": [
        {
            "time": "2019-09-26T08:00:00Z",
            "value": 1.45
        },
        ...
]
```
