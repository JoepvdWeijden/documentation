# 2. User Guide

The user guide helps you deploy your data to production. Before going through the user guide, you should look at [Quick Start](QUICK-START.md) guide for details on the software setup and testing.

The User Guide covers the following topics:
1. [Kafka](#kafka): Use the Kafka REST API to send a test message
2. [Time series database](#influxdb) (influxDB): Check real time data
3. [Object store](#minio) (Minio): Check historical data
4. [Metadata database](#central-metadata-database-postgresql) (PostgreSQL): Browse metadata
5. [Dashboard](#project-dashboarding-service-grafana) (Grafana): Visualise test data

Your project must be [onboarded](ONBOARDING.md) onto the Green Village data platform to continue. To send and retrieve data you will need `your_project_name`, `your_project_password` and `your_write_password` received after onboarding.

#### Quick links to project resources:
- **InfluxDB**: https://PROJECT_NAME-influxdb.green-village.sda-projects.nl
- **Minio**: https://PROJECT_NAME-minio.green-village.sda-projects.nl
- **Grafana**: https://PROJECT_NAME-grafana.green-village.sda-projects.nl


## Setup

#### Export environment variables with the credentials

```
export PROJECT_NAME=your_project_name
export PROJECT_PASSWORD=your_project_password
export WRITE_PASSWORD=your_write_password
```

#### First test, then deploy
Every device from every project should first send a test messages to the staging environment (as explained in the [Quick Start](QUICK-START.md) guide). Only after validating the data quality should resources be deployed to production.

## Overview of user roles

The user roles are summarised in the table below. In Minio user roles are called policies. As a general rule, only the admin user will create new users, and these will typically be project-read or project-write users.

| User | Kafka | InfluxDB | Minio | Project Grafana | PostgreSQL |
|-|-|-|-|-|-|
|`admin`|full access | full access | full access | full access | full access |
| `pipeline` | N/A | read/write access for automated data pipeline | read/write access for automated data pipeline. Restricted to GET and PUT operations on predefined buckets. | N/A | read/write access for automated data pipeline |
| `project-read` | consume (read) messages | read-only access | read-only access for predefined buckets. User can create new buckets with full access to them. | read-only access | read-only access |
| `project-write` | produce (write) messages | N/A | N/A | read/write for project folders, read-only for data source | N/A |
| `public` | N/A | N/A | N/A | read-only for public folders | N/A |

## Kafka
Each project can send data into the platform by producing to the project's Kafka topic. The topic has a schema associated with it and it will reject any messages not compliant with the [generic schema](DATA.md#generic-schema).

### Produce/consume messages
There are various ways of producing and consuming. Depending on your use case and level of expertise, you may prefer one over the other. Examples and additional documentation for producing/consuming can be found in the [examples repository](https://gitlab.com/the-green-village/examples)

## InfluxDB
Messages produced to the topic are stored in a project-specific InfluxDB. Each type of value in a message is stored in its own time series.

### Interface
Data can be accessed using the [influx command line client](https://docs.influxdata.com/influxdb/v1.7/tools/shell/).

First, connect to the InfluxDB for your project, given as `${PROJECT_NAME}-influxdb.$K8S_HOST`:

```sh
influx -host $PROJECT_NAME-influxdb.$K8S_HOST \
    -port 443 -ssl \
    -username USERNAME \
    -password PASSWORD \
    -database ${PROJECT_NAME} 
```

Next, show the available measurements and series:

```
> SHOW MEASUREMENTS
> SHOW SERIES
```

Make sure timestamps are shown human-readable:

```
> precision rfc3339
```

Some example queries:

```
> SELECT * FROM /.*/
> SELECT LAST(*) FROM /.*/
> SELECT * FROM /.*/ ORDER DESC LIMIT 1
> SELECT value FROM /.*/ WHERE device_id = 'test'
```

Inspect tag and field keys:

```
> SHOW TAG KEYS
> SHOW TAG VALUES WITH KEY = device_id
> SHOW FIELD KEYS
```

The influx client can be used to download data in a CSV or JSON format:

```sh
> influx -host $PROJECT_NAME-influxdb.$K8S_HOST \
    -port 443 -ssl \
    -username USERNAME \
    -password PASSWORD \
    -database ${PROJECT_NAME} \
    -precision rfc3339 \
    -execute "SELECT value FROM /.*/ WHERE device_id = 'device1'" -format csv
name,time,value
test_device1_temperature,2019-09-19T14:05:01Z,42
test_device1_temperature,2019-09-19T14:05:22Z,42
test_device1_temperature,2019-09-19T14:06:08Z,42
test_device1_temperature,2019-09-19T14:06:18Z,42
test_device1_temperature,2019-09-19T14:08:57Z,42
```

Alternatively, one can interact with the database through [InfluxDB HTTP endponts](https://docs.influxdata.com/influxdb/v1.7/tools/api/). An example query is given below.

```sh
> curl "https://$PROJECT_NAME-influxdb.$K8S_HOST/query?u=$PROJECT_NAME&p=$PROJECT_PASSWORD" \
    --data-urlencode "db=$PROJECT_NAME" \
    --data-urlencode "q=SHOW MEASUREMENTS"
```

## Minio
An isolated Minio instance is available as long-term data storage for each project. Data is stored in Avro format in the `kafka-connect` bucket.
More information about the data formats can be found in the [data documentation](DATA.md).

### Interface
The Minio instance has a web UI provided by the URL generated during project bootstrapping as `https://${PROJECT_NAME}-minio.$K8S_HOST`. For command-line access, use the [mc client](https://github.com/minio/mc). To initialise the Minio client with the project Minio instance, use Minio config:

```sh
> mc config host add ${PROJECT_NAME}-green-village \
    https://${PROJECT_NAME}-minio.$K8S_HOST \
    MINIO_ACCESS_KEY MINIO_SECRET_KEY
```

Example usage:
- List files:
```sh
mc ls $PROJECT_NAME-green-village --recursive
```

- Copy files:
```sh
mc cp --recursive $PROJECT_NAME-green-village/kafka-connect .
```

More information on the use of the client can be found in the [Minio documentation](https://docs.min.io/docs/minio-client-complete-guide).


## Project dashboarding service (Grafana)
A per-project Grafana dashboard can be accessed through the project's dashboarding service URL, given as https://PROJECT_NAME-grafana.$K8S_HOST. Dashboards in Grafana are organised in folders. Two folders are provisioned: private and public.

The **private folder** comes with a preconfigured dashboard with a number of metrics relevant for monitoring the data ingestion and Minio and Kafka health. In addition, all devices and measurements are loaded dynamically from the project's InfluxDB instance, and can be explored in the private dashboard on the fly. All InfluxDB data sources are defined in Grafana, with read access rights on the project basis.

The **public folder** is empty by default. **Dashboards created in the public folder are publicy exposed, and accessible without login.**

The project users can create their own dashboards to visualise data, see the [Grafana documentation](https://grafana.com/docs/) for more details.


## Central metadata database (PostgreSQL)
A central metadata database is in place to enable convenient data discovery and management. For every data file in Minio, metadata is extracted and centrally stored in a PostgreSQL database available at `postgresql.$K8S_HOST`, using a dedicated [data model](DATA.md#metadata-extraction-and-the-data-model-for-the-central-metadata-database).

The central metadata database is accessible to all projects.
Two instances of the metadata database exist in PostgreSQL:
- `thegreenvillage` for production metadata and
- `test` only for the staging data sent to the Kafka topic of the `test` project.

### Interface
Metadata can be accessed using the [psql interactive terminal](https://www.postgresql.org/docs/9.2/app-psql.html).

First, connect to the database at `postgresql.$K8S_HOST`.
```sh
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d thegreenvillage
```

Example commands:
```
\dt
SELECT * FROM devices
```

More advanced queries can be used to answer questions such as:
* Which devices belong to project X?
```
SELECT device_id FROM devices WHERE project_id='X';
```
* List all the files for device Y.
```
SELECT filename FROM files WHERE device_id='Y';
```
* Which devices measure quantity Z?
```
SELECT device_id FROM devices WHERE uid IN (
  SELECT device_uid FROM device_quantity WHERE quantity_uid IN (
    SELECT uid FROM quantities WHERE name='Z'
  )
);
```
* Which quantities are measured in project X?
```
SELECT * FROM quantities WHERE uid IN (
  SELECT quantity_uid FROM device_quantity WHERE device_uid IN (
    SELECT uid FROM devices WHERE project_id='X'
  )
);
```
* Which quantities are measured in device Y?
```
SELECT * FROM quantities WHERE uid IN (
  SELECT quantity_uid FROM device_quantity WHERE device_uid IN (
    SELECT uid FROM devices WHERE device_id='Y'
  )
);
```
* How many messages did device Y measure on a particular day?
```
SELECT messages FROM files
WHERE device_id='Y'
AND date='YYYY-MM-DD';
```
