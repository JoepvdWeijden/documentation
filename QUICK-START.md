# 1. Quick Start
This quick start guide helps you to install software on your local machine, e.g. laptop, to interact with the Green Village (GV) platform, helps you to send a first message to the staging or test environment of the GV Platform and shows you how to check the sent message in the different data stores of the GV Platform. Note, that your project must be [onboarded](ONBOARDING.md) onto the Green Village data platform before you can proceed. If you have succeeded of sending a message and you are happy with the quality of the data you can go to the production environment. For documentation on setting up your production pipeline, see the [User Guide](USAGE.md). 

The quick start guide will take you through the following steps:
1. Software and environment setup
2. [Kafka](#send-a-test-message-to-kafka): Use the Kafka REST API to send a test message
3. [Time series database](#time-series-database-influxdb-check-the-real-time-data) (influxDB): Check real time data
4. [Object store](#object-store-minio-check-historical-data) (Minio): Check historical data
5. [Metadata database](#metadata-database-postgresql-browse-metadata) (PostgreSQL): Browse metadata
6. [Dashboard](#dashboard-grafana-visualise-test-data) (Grafana): Visualise test data

## Setup

#### Required Software

* install [influxdb oss](https://docs.influxdata.com/influxdb/v1.8/introduction/install/)
* install [minio client](https://docs.min.io/docs/minio-client-quickstart-guide.html)
* install [postgreSQL](https://www.postgresql.org/download/)
* install [jq](https://stedolan.github.io/jq/) (`sudo apt install jq`)

#### Export environment variables with the credentials
A result of the onboarding process is that you will get credentials on how to log in into the test environment. You will need `your_project_name`, `your_project_password` and `your_write_password` to send and retrieve data. You will also receive credentials for the production environment. You will need them for the next tutorial [User Guide](USAGE.md).

```
export PROJECT_NAME=your_project_name
export PROJECT_PASSWORD=your_project_password
export WRITE_PASSWORD=your_write_password
export K8S_HOST=k8s_dataplatform_server
```

## Send a test message to Kafka

#### Prepare the test message

The message **must** be in the generic schema format. The easiest way of ensuring the correct schema is to obtain it from the Schema Registry. In this example the schema is stored in the environment variable `SCHEMA_ID`.

```
export SCHEMA_ID=$(curl -s https://schema-registry.$K8S_HOST/subjects/test-schema-value/versions/latest | jq .id)

echo '{
  "value_schema_id": '$SCHEMA_ID',
  "records": [{
      "value": {
        "data": {
          "timestamp": '$(date +%s)'000,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "test",
          "values": [{
              "description": "temperature",
              "name": "temperature",
              "type": "INT",
              "unit": "degrees",
              "value": {
                "int": 42
              }}]},
        "metadata": {
          "description": "test device",
          "location": null,
          "latitude": null,
          "longitude": null,
          "altitude": null,
          "type": null,
          "manufacturer": null,
          "placement_timestamp": null,
          "project_id": "'$PROJECT_NAME'",
          "device_id": "test",
          "serial": null
        }}}]}' > message.json
```

#### Send the message to Kafka

The example below shows how to send a message using the Kafka REST Proxy. Since we want to use the staging (i.e. test) environment, we specify the `test-schema` topic.

```
curl -X POST -H "Content-Type: application/vnd.kafka.avro.v2+json" \
  -H "Accept: application/vnd.kafka.v2+json" \
  -u $PROJECT_NAME-write:$WRITE_PASSWORD \
  --data @message.json \
  https://$PROJECT_NAME-kafka-rest.$K8S_HOST/topics/test-schema | jq .
```

  If you get a ssl error, add `-k` after `POST` and before `-H`. On success, you should get a response like this.

```
{
  "offsets": [{
      "partition": 1,
      "offset": 108,
      "error_code": null,
      "error": null
    }],
  "key_schema_id": null,
  "value_schema_id": 41
}
```

## Time series database (InfluxDB): Check the real-time data 
After the message is sent, data immediately appears in the time series database. Launch InfluxDB in your terminal with `influxd`. The last messages from the database belonging to the test project can be queried (in a new terminal window) like this.

```
influx -host test-influxdb.$K8S_HOST -port 443 \
  -ssl -username $PROJECT_NAME -password $PROJECT_PASSWORD -database test \
  -execute "SELECT * FROM /$PROJECT_NAME.*/ ORDER BY DESC LIMIT 1" \
  -precision rfc3339
```

Here is an example output of the query above.

```
name: test_test_temperature
time                 description device_description device_id name        project_id type unit    value
----                 ----------- ------------------ --------- ----        ---------- ---- ----    -----
2020-02-12T07:40:02Z temperature test device        test      temperature test       INT  degrees 42
``` 

The InfluxDB test environment can be accessed online at `https://test-influxdb.green-village.$K8S_HOST.nl`. You'll need your project name and password to sign in.

## Object store (Minio): Check historical data

The object store is updated on a regular basis with the latest batch of messages that arrived to the platform. For the test environment, the object store is updated every minute or after every three messages. For the production environment the object store is updated every 24 hours. The latest raw files are in the `kafka-connect` bucket. If the messages in the latest file have not been seen before, they will be written automatically into JSON and CSV files, and stored in the `data` and `csv` buckets respectively. Files in the data and csv buckets are organised by device and day. The raw files are in the serialized `avro` format and are suitable for programmatic analysis.

```
mc config host add test-green-village https://test-minio.$K8S_HOST/ $PROJECT_NAME $PROJECT_PASSWORD
mc ls test-green-village/kafka-connect --recursive | sort | tail
```

After decoding, the raw files contain messages in the generic schema format.

```
{"data": {"device_id": "test", "project_id": "test", "timestamp": 1581493202000, "values": [{"description": "temperature", "name": "temperature", "type": "INT", "unit": "degrees", "value": 42}]}, "metadata": {"altitude": null, "description": "test device", "device_id": "test", "latitude": null, "location": null, "longitude": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": null, "type": null}}
{"data": {"device_id": "test", "project_id": "test", "timestamp": 1581493742000, "values": [{"description": "temperature", "name": "temperature", "type": "INT", "unit": "degrees", "value": 42}]}, "metadata": {"altitude": null, "description": "test device", "device_id": "test", "latitude": null, "location": null, "longitude": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": null, "type": null}}
{"data": {"device_id": "test", "project_id": "test", "timestamp": 1581493755000, "values": [{"description": "temperature", "name": "temperature", "type": "INT", "unit": "degrees", "value": 42}]}, "metadata": {"altitude": null, "description": "test device", "device_id": "test", "latitude": null, "location": null, "longitude": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": null, "type": null}}
```

On demand, the raw files can be translated into CSV files. Here is an example of the contents of a CSV file.

```
# Metadata
# {"description": "test device", "device_id": "test", "location": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": null, "type": null}
# Quantities
# {"description": "temperature", "name": "temperature", "type": "INT", "uid": 1, "unit": "degrees"}
,,,,temperature
,,,,temperature
,,,,degrees
,,,,INT
timestamp,time,project_id,device_id,1
1581493202000,2020-02-12 07:40:02.000000,test,test,42
1581493742000,2020-02-12 07:49:02.000000,test,test,42
1581493755000,2020-02-12 07:49:15.000000,test,test,42
```

The Minio test environment can be accessed online at `https://test-minio.green-village.$K8S_HOST.nl`. You'll need your project name and password to sign in.

## Metadata database (PostgreSQL): Browse metadata

The metadata give an overview of all the projects and devices in the platform, together with the information on what quantities are measured and what data files with how many messages are available. The metadata database is updated at the same frequency as the object store, but is filled only after files are saved in the object store. It can be accessed with the project password. The `-d test` flag specifies the "test" database in the test environment. 

```
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c "SELECT * FROM projects"
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c "SELECT * FROM devices"
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c "SELECT * FROM metadata"
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c "SELECT * FROM quantities"
```

#### Examples of more advanced queries

##### Which quantities are measured by your project?
```
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c """
  SELECT * FROM quantities WHERE uid IN (
    SELECT quantity_uid FROM device_quantity WHERE device_uid IN (
      SELECT uid FROM devices WHERE project_id='$PROJECT_NAME'
    )
  );"""
```
```
 uid |    name     | description |  unit   | type
-----+-------------+-------------+---------+------
   1 | temperature | temperature | degrees | INT
```
##### Which devices from the project measure temperature?
```
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c """
  SELECT device_id, project_id FROM devices WHERE uid IN (
    SELECT device_uid FROM device_quantity
    WHERE quantity_uid IN (
      SELECT uid FROM quantities WHERE name='temperature'
    )
    AND project_id='$PROJECT_NAME'
  );"""
```
```
 device_id | project_id
-----------+------------
 test      | test
```
##### How many messages are stored in each file for the project?
```
PGPASSWORD=$PROJECT_PASSWORD psql --host postgresql.$K8S_HOST --port 9998 -U $PROJECT_NAME -d test -c """
  SELECT filename, messages FROM files WHERE project_id='$PROJECT_NAME'"""
```
```
            filename            | messages
--------------------------------+----------
 test-test-2020-02-12.json      |        3
```

## Dashboard (Grafana): Visualise test data

The time series database from the test project is availale as a data source in the project's [Grafana dashboard](https://grafana.green-village.$K8S_HOST.nl/).
