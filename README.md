<span style="padding-bottom: 1em;">
	<img src="images/surf-logo.png" style="height: 4.5em; float: left; margin-top: 1em; margin-left: 2em;"/>
	<img src="images/the-green-village-logo.png" style="height: 4.5em; float: right; margin-bottom: 1em;"/>
<span/>
<hr style="clear: both; margin-top: 7em;"/>

# The Green Village Data Platform
The Green Village Data Platform is an open-source platform for storing, monitoring, visualising and sharing of sensor data from [The Green Village](https://www.thegreenvillage.org/). The platform aggregates real-time data streams of the different [projects](https://www.thegreenvillage.org/projects) and makes them available to many users and applications in a controlled and secure collaborative environment. New projects can be onboarded quickly and with a minimal amount of work, providing  project-based access to and sharing of new data.

The platform is developed by [SURF](https://www.surf.nl/en/research-ict) with open source libraries and tools, and deployed as a fault-tolerant, highly-available service on a [Kubernetes](https://kubernetes.io/) cluster running on SURFsara premises. To get started with the platform, take a look at the [platform overview](OVERVIEW.md) or these [slides](TGV_data_platform.pdf). New to the platform and want to add your project? Then take a look at the [onboarding process](ONBOARDING.md) and get in touch!

## Documentation Overview
More details can be found on dedicated pages:
1. [Quick start](QUICK-START.md): Send messages to the platform and look at the data (beginner)
2. [User guide](USAGE.md): Detailed examples on using the platform
3. [Public API](PUBLIC-API.md): Access the publicly exposed time series data
4. [Data section](DATA.md): Information on the data flows and data formats
5. [Data sharing guide](SHARING.md): Share data between projects
6. [Cluster installation guide](CLUSTER-INSTALLATION.md) (advanced)
7. [Project installation guide](PROJECT-INSTALLATION.md) (advanced)

The [Quick start](QUICK-START.md) and the [User guide](USAGE.md) are for the user who wants to connect her project/device(s) to the Green Village platform. The [Cluster installation guide](CLUSTER-INSTALLATION.md) and [Project installation guide](PROJECT-INSTALLATION.md) are for the administrators of the Green Village platform or users who want to set up their own platform and create projects on the cluster. The other three manuals of point 3-5 can be read as background by users and administrators of the platform. For the platform user the most relevant parts of these manuals have a reference in the (QUICK-START.md) and the [User guide](USAGE.md). In the future we expect another type of user, namely the data scientist. This is a user that will use the data streams of different the connected project for their research. This user will be particularly interested in the manuals at point 3-5. However manaul 5 has to be extended.

## Platform Overiew
The diagram and description below provide a quick summary of the platform. 
![The Green Village Data Platform at a glance](images/overview.png)

### Kafka cluster
The core functionality of the platform is provided by [Apache Kafka](https://kafka.apache.org/), a fault-tolerant and scalable distributed streaming platform. The highly-available configuration of Kafka allows for high-throughput data processing, with many projects and sensors interacting with the platform at any given time.

Messages sent into Kafka follow a common format, which enables automatic aggregation, metadata extraction and visualisation using the [automated data pipeline](DATA.md#automated-data-pipeline). Data streams can be shared with other projects.

### Object store
All data is stored in [Minio](https://min.io/), an S3-compatible object store. Minio provides several interfaces for data manipulation, among them a web user interface.

Each project will store data in its own Minio *instance*, isolated from all other projects. The platform will store raw data, as well as data in formats designed for interfacing with programming languages and spreadsheet editors, made possible with the [automated data pipeline](DATA.md#automated-data-pipeline).

### Time series database
All data with a schema is automatically transformed and inserted into [InfluxDB](https://www.influxdata.com/) using the [automated data pipeline](DATA.md#automated-data-pipeline). InfluxDB is a time series database providing access to all produced data using a SQL-like query language called InfluxQL.

Each project stores data for its own time series in an isolated InfluxDB database. This database can be shared with other projects, and specific time series can be made public to expose them in the **TODO: LINK 3D visualisation**.

### Metadata database
Metadata is automatically aggregated in a central metadata database using the [automated data pipeline](DATA.md#automated-data-pipeline). This database can be queried for the different measurements and devices for each project, as well as the the number and size of the messages received by a particular device on a particular day.

### Dashboards
For monitoring platform health, a number of dashboards are defined in the central [Grafana](https://grafana.com/) monitoring component. Cluster-wide dashboards include those monitoring the different platform components (Kafka, Minio) and the underlying infrastructure (Kubernetes).

In addition, each project will have its own, project-specific Grafana instance. This instance will have a monitoring dashboards that will automatically expose measurements for all devices in that project. The dashboard will show data stream statistics and health metrics for the different components such as the object store and time series database.

Alerts are automatically created for all cluster-wide and project-specific dashboards, allowing for rapid response on failure.
