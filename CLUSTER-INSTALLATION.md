# Cluster installation
This page describes how to install the core components of The Green Village Data Platform on a [Kubernetes](https://kubernetes.io/) cluster. The Green Village Data Platform uses Kubernetes to deploy all central and project resources for high availability, robustness and security.

This page does not cover the installation or maintenance of the Kubernetes cluster itself. Kubernetes can be [installed manually](https://kubernetes.io/docs/setup/) on a number of machines, by using [Rancher](https://rancher.com/docs/rancher/v2.x/en/installation/), or through one of the many managed Kubernetes environments provided by, for example, [Google Cloud](https://cloud.google.com/kubernetes-engine/), [Amazon EKS](https://aws.amazon.com/eks/) or [Rancher](https://rancher.com/products/rancher/).

We do not advise the use of local Kubernetes development environments such as [minikube](https://github.com/kubernetes/minikube), [Docker Desktop](https://docs.docker.com/docker-for-mac/kubernetes/), or [k3s](https://k3s.io/). Although great for learning and developing on These environments will only run on a single machine without many high availability and security measures in place, and are not production grade.

**NOTE: please be aware that the installation of a Kubernetes cluster and the platform itself is not trivial**. Although we have tried to make sure the installation is as straightforward as possible, your cluster configuration may differ from the clusters we have developed and tested on. System administration knowledge may be required to deal with any problems effectively.

## Prerequisites
Before installing the platform on a Kubernetes cluster, please verify you have setup the cluster and your local environment as described in the following sections.

### Cluster resources
The platform hosts a large number of components, many of them costly in terms of CPU and memory requirements. For this reason, we advise a cluster of at least 5 (virtual) machines for hosting the platform's core and project components, in addition to separate nodes hosting the Kubernetes [control plane](https://kubernetes.io/docs/concepts/#kubernetes-control-plane) and [etcd](https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/).

All nodes should run the same version of Kubernetes, version 1.14.6 at minimum. Each should have at least 4 CPUs and 8 GB of memory.

In addition, the following components need to be installed on the Kubernetes cluster:

* A storage provisioner, with a default storage class set
* The [Kubernetes metrics server](https://kubernetes.io/docs/tasks/debug-application-cluster/resource-metrics-pipeline/)
* The [NGINX ingress controller](https://github.com/nginxinc/kubernetes-ingress)
* The [cert-manager](https://github.com/jetstack/cert-manager) for generating [Let's Encrypt](https://letsencrypt.org/) TLS certificates for HTTPS ingress

### Command line tools
The platform's installation scripts use a number of tools. Please make sure [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and [Helm](https://github.com/helm/helm). Please make sure both are installed and are configured to interface with the cluster, and are availabe on the command line.

In addition, please make sure you have the following additional tools installed and available on the command line:

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), for cloning the platform's repository
- [mo](https://github.com/tests-always-included/mo), for templating functionality
- [figlet](http://www.figlet.org/), for feedback during platform installation
- [cowsay](https://github.com/tnalpgge/rank-amateur-cowsay), also for feedback during platform installation

## Installation guide
The installation will deploy the following components on the cluster:

- General setup (TCP ports)
- SSH bastion pod
- Monitoring (Prometheus, Grafana and monitoring dashboards) 
- Kafka brokers and ZooKeeper
- Schema Registry
- PostgreSQL for the research data management and metadata
- OpenFaaS
- OpenFaaS functions to organise data and extract metadata
- Resources provided to individual projects:
  - Minio for long-term storage with webhooks to OpenFaaS
  - InfluxDB as a data source for Grafana dashboards
  - Grafana with data source, folder and monitoring dashboard
  - Kafka topics, ACLs and generic schema
  - Automated data pipeline (Kafka to InfluxDB)
  - Kafka Connect and S3 Sink Connectors
  - Kafka REST Proxy
- Flask project management and data sharing API

To start the installation process, clone The Green Village Data Platform [repository](https://gitlab.com/the-green-village/tgv-bootstrap) from Gitlab:

```sh
$ git clone https://gitlab.com/the-green-village/tgv-bootstrap
$ cd tgv-bootstrap/core
```

Please follow the instructions in the `README.md` files in the `tgv-bootstrap/` directory to install the platform.
