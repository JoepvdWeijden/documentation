# Project installation
This page describes how a **cluster administrator** can create new projects in The Green Village Data Platform. We will use the project administration API to create an example project, and describe the components created on the Kubernetes cluster as a result.

## Creating a new project
A new project can be created by calling the appropriate path on the project administration API at [https://flask.green-village.sda-projects.nl/project/](https://flask.green-village.sda-projects.nl/project/). The project name is specified as a path component, and the password for the project user with the `password` item in a JSON file. Let's create this JSON file first:

```sh
$ echo '{"password": "ut9LeSha"}' > password.json
``` 

Now create the project named `test` with the password for its project user as above:

```sh
$ curl -X POST \
  "https://flask.green-village.sda-projects.nl/project/test" \
  -u admin \
  -H "accept: application/json" \
  -H "Content-Type: application/json" \
  -d@password.json
```

This API call will trigger an installation script that will take several minutes to finish. Afterwards, verify the project has been created successfully by checking the project is in the list of projects from the API at [https://flask.green-village.sda-projects.nl/project](https://flask.green-village.sda-projects.nl/project):

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/project" \
  -u admin \
  -H "accept: application/json"
```

## Components created during project installation
During project installation, a number of components will be created and configured. The rest of this page describes in detail what happens exactly in this process. We will assume you have created the project called `test` as described above. The components listed below are configured for this project as an example.

The Kafka components are deployed on a Kubernetes cluster using [Helm charts from Confluent](https://github.com/confluentinc/cp-helm-charts).

### The project user and pipeline user
A Kubernetes secret will be created containing the **project user**'s username and password. The username will be identical to the project name, and the password as provided in the first API call above. These credentials are used to authenticate to the different project's services and components. Whenever we talk about the **project user** in the remainder of this page, it will have credentials as described above.

The **pipeline user** is used by the [automated data pipeline](DATA.md#automated-data-pipeline) to interface with the different components of the cluster. Credentials for this user will be the same for each component for a single project.

### PostgreSQL metadata database
A **project user** will be created in the central PostgreSQL metadata database, with **read-only** access to the metadatabase. This metadatabase can be queried by following the steps in the [usage documentation](USAGE.md#central-metadata-database-postgresql).

### Minio object store
A project-specific Minio instance will be created on the Kubernetes cluster. This instance will be created as a Kubernetes deployment, along with an external HTTPS ingress listening on URL `https://test-minio.green-village.sda-projects.nl/`. **Project user** credentials are needed to log in to the web interface exposed on the ingress.

The project's Minio instance will be configured with two users:

1. The **project user**, with access key equal to the project's username and secret key equal to the password. The project user can read from the pre-existing buckets as described in the [data documentation](DATA.md), and can create new buckets with full access
1. The **pipeline user**, with **full access** to the instance

In addition, several automated data pipeline hooks will be installed that are triggered by new data uploaded to Minio. These are described as the actions in the [automated data pipeline documentation](DATA.md#automated-data-pipeline).

Lastly, the installation creates a Kubernetes secret containing the **pipeline user** credentials for Minio. These are used by the OpenFaaS functions of the automated data pipeline to interface with Minio.

### InfluxDB time series database
A project-specific InfluxDB instance will be created on the cluster. This instance will host time series data in a database named identical to the project name (in our case: `test`), **with a retention period of 90 days**. The installation will create an InfluxDB deployment on Kubernetes, with an external ingress for [InfluxDB API](https://docs.influxdata.com/influxdb/v1.7/tools/api/) access over HTTPS. The instance will be accessible from the outside on the URL `https://test-influxdb.green-village.sda-projects.nl`. **Project user** credentials are required for interfacing with the API.

In addition, two users will be created for this database:

1. The **project user**, with **full read-only access**
1. The **pipeline user**, with **full access**

### Grafana dashboards
The Grafana installation comprises the following steps:

1. Creation of a project-specific Grafana deployment on the Kubernetes cluster
1. Creation of a project-specific ingress to this deployment at `https://test-grafana.green-village.sda-projects.nl`. **Project user** credentials are required for login.
1. Creation of a **project user** in the Grafana instance
1. Creation of a [data source](https://grafana.com/docs/features/datasources/) for the project's InfluxDB instance
1. Creation of a private folder for the project
1. Setting permissions on the private folder such that it is only readable by the **project user**
1. Creation of a public folder for the project
1. Setting permissions on the public folder such that is **exposed to and readable by the outside world**
1. Create a default dashboard in the private folder

The default dashboard will show all devices and measurements in the project, and allow for dynamic data exploration. Furthermore, it will show various metrics on the peformance of the project-specific components, such as the incoming messages per second and the remaining disk space for Minio and InfluxDB.

#### Alerting
The following alerts will be sent to the notification channels defined by the **cluster administrator**:

1. No data has been sent to the project's topic **for the last hour**
1. The Kafka Connect instance should be running
1. The remaining disk space for Minio should be **above 25%**
1. The remaining disk space for InfluxDB should be **above 25%**

### Kafka
Kafka will be configured as follows:

1. A new schema for the project will be registered in the schema registry, named `test-schema-value`
1. A **project user** will be created
1. The **project user** is added to the ACLs with producer capability and consumer capability, where a consumer group is required to be specified
1. A project topic is created called `test-schema`, with three partitions and a replication factor of three

### Automated pipeline configuration
A function will be deployed using OpenFaaS to automatically convert raw message data uploaded to Minio to its JSON representation. Please see the [automated data pipeline documentation](DATA.md#automated-data-pipeline) for details.

### Kafka to InfluxDB deployment
A Kubernetes deployment will be created that will read data from the project's topic, and send it to the project's InfluxDB instance. Two steps are needed for this:

1. Creation of a ConfigMap with the relevant information to be able to consume data from Kafka, and to send it to InfluxDB. Credentials for the **pipeline user** will be used for this.
1. Creation of the deployment itself

### Kafka Connect instance
A project-specific Kafka Connect instance will be created as a deployment on the Kubernetes cluster. This instance will consume data from the project's topic with **project user** credentials and upload it to the `kafka-connect` bucket of the project's Minio instance with **pipeline credentials**.

By default, messages will be flushed to Minio either if 1000 messages have been received, or 12 hours have elapsed. As a consequence, there is delay between data being sent to the project's topic and the data appearing in Minio. This does not affect the topic itself, or the project's InfluxDB - both data sources can be read from in real-time.

### Kafka REST Proxy
A project-specific REST Proxy instance will be created as a deployment on the Kubernetes cluster. A project-specific external ingress will be created on the cluster that will expose the REST Proxy instance to the outside world. This ingress is located at `https://test-kafka-rest.green-village.sda-projects.nl`. **Project user** credentials are required for basic authentication.

Ingress credentials are stored as a secret on the Kubernetes cluster.
