# Sharing data
This document describes how to share data between projects. The **cluster administrator** can share data between projects, **read-only**, and make data publicly available. Different types of data can be shared:

1. [Sharing data files](sharing/SHARING-MINIO.md) from Minio between projects
1. [Sharing data streams](sharing/SHARING-KAFKA.md) from Kafka between projects
1. [Sharing time series data](sharing/SHARING-INFLUXDB.md) from InfluxDB between projects
1. Making individual time series data [public](sharing/SHARING-PUBLIC.md)

The sharing possibilities are summarised in the table below:

| **Data store** | **what is shared** | **with whom**
| :---- | :---- | :---- |
| Kafka | topic | between two projects |
| Minio | files based on wildcards | between two projects |
| InfluxDB | full database | between two projects |
| InfluxDB | individual time series | exposed publicly |

All sharing functionality is enabled by the cluster management HTTP API located at [https://flask.green-village.sda-projects.nl/](https://flask.green-village.sda-projects.nl/). This web page will provide a list of exposed methods and additional documentation.
