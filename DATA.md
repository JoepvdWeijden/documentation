# Data

As messages arrive in Kafka, the [automated data pipeline](#automated-data-pipeline) takes care of automatic re-distribution into other platform's storage services. In order to enable the automated processing, the incoming data has to be standardised (see the section on the [generic schema](#generic-schema).

This section describes all the data formats used in The Green Village data platform and explains how data is organised/stored at different levels. 
The following table gives a high-level overview.

| **Data store** | **Data format** | **Purpose** | **Availability of new data** | **Retention period** |
| :---- | :---- | :---- | :---- | :---- |
| Kafka | Avro | Real-time data consumption | real-time | 7 days |
| InfluxDB | Time series | Visualisation and time series queries | real-time | 90 days |
| Minio | Avro | Analysis of raw sensor data | in batches | long-term |
| Minio | CSV | Manual analysis of sensor data and metadata (e.g. with a spreadsheet editor) | in batches | long-term |
| PostgreSQL | | Central metadata catalogue | in batches | long-term |


## Generic schema

The data entering The Green Village data platform is required to be compatible with a dedicated format. This data format - so called **generic schema** - is designed to accomodate various kinds of sensor measurements together with the information about the data source. The former belongs in the data section of the generic schema, the latter in the metadata section.

**Data:**
- `project_id`: Globally unique identifier for the project.
- `device_id`: Unique identifier for the device within a project.
- `timestamp`: Timestamp (milliseconds since unix epoch) of the measurement.
- `values`: List of the measured values in the format as described below. In principle, devices can have multiple sensors and thus measure multiple quantities.

**Value:**
- `name`: Short name of the measured quantity. This will be used as an identifier in the database and should not contain any spaces or special characters.
- `description`: Human-readable short description of the measured quantity.
- `unit`: Unit of the quantity. Leave an empty string in case the quantity does not have a dimension.
- `type`: Type of the measured value. The options are: NULL, BOOLEAN, INT, LONG, FLOAT, DOUBLE or STRING.

**Metadata:**
- `project_id`: Globally unique identifier for the project.
- `device_id`: Unique identifier for the device within a project.
- `description`: Human-readable short description of the device.
- `type` (optional): Type of the device.
- `manufacturer` (optional): Manufacturer of the device.
- `serial` (optional): Serial number of the device.
- `location` (optional): Description of the location the device is placed at.
- `latitude` (optional): Latitude in decimal degrees of the device location (GPS coordinates). This is for stationary devices only (moving devices should place the coordinates into the data section).
- `longitude` (optional): Longitude in decimal degrees of the device location (GPS coordinates). This is for stationary devices only (moving devices should place the coordinates into the data section).
- `altitude` (optional): Altitude in meters above the mean sea level. This is for stationary devices only (moving devices should place the coordinates into the data section).

The data and metadata sections have the `project_id` and `device_id` fields in common. This is to provide a link between the two sections. Every measurement sent to the platfrom needs to contain both sections and the `project_id` and `device_id` need to be comptaible. The `project_id` and `device_id` fields uniquely define a deivce. Together with `timestamp`, the three fields uniquely define a measurement.

Sending metadata with every message is a design choice. This is to ensure that every standalone message can be fully interpreted. However, this comes at a cost because it results in an increased size of the raw messages stored in the object store, where every message from a given device contains the same metadata. Given the size of the messages, this is not considered as a waste of storage resources.

In principle, different metadata can be sent for a device with a unique pair of `project_id` and `device_id`. This is allowed because the device can be updated (e.g. with a new serial number in case it is replaced, or a device can be moved to a different place). However, it should be avoided to modify metadata too often. By design, metadata should hold static information about the device and the information changing in time should go into the data section. Therefore, it is important to keep in mind that the metadata fields for the location coordinates should be used only for stationary devices.

Examples and additional documentation for producing messages to Kafka can be found in the [examples repository](https://gitlab.com/the-green-village/examples).


## Automated data pipeline

An automated data pipeline is in place to re-distribute the data entering the platform into the platform's storage components. The automated pipeline is realized in two stages: acting on the data arriving in Kafka, and acting on the data files arriving in the Minio object store.

**Acting on Kafka messages:**
- Write every incoming message one-by-one to the InfluxDB time-series database.
- Write messages in batches into the object store. These batches are written as a single file either every hour or once a certain number of messages arrive within a single project.

**Acting on the files in the Minio object store:**
- Extract metadata for every new batch of messages arriving in the object store.

The second stage of the automated pipeline only takes place after a new batch of messages is stored in Minio. Therefore, is important to keep in mind that the products of this second stage are not available immediately.

![Automated data platform](images/automated-data-pipeline.png)

**On-demand:**
- Copy the messages from the raw files into CSV files.


## InfluxDB fields and tags

Data points in InfluxDB arrive in real time and the intention is to use them primarily for visualisations. Data is kept in InfluxDB for 90 days.

The generic schema supports sending multiple quantities in a single message. Every quantity from a message is stored in InfluxDB as an independent *measurement*.
Each *measurement* is defined by `project_id`, `device_id` and `name` of the value. The name of the *measurement* is in the format `(project_id)_(device_id)_(name)` (without the brackets), all in lowercase. The InfluxDB *measurement* entries consist of time, fields and tags. Tags are useful to quickly organise *measurements* into separate *series* in InfluxDB.
- Time is taken from the `timestamp` data field, i.e. it is the actual time of the measurement, not the time of the arrival of the message in the platform.
- Fields:
  - `value` of the value from the data section,
  - `latitude`, `longitude`, `altitude` from metadata (used for stationary devices only, moving devices should their coordinates as data values).
- Tags: 
  - `project_id`, `device_id`, `description`, `type`, `location` from the metadata section,
  - `name`, `description`, `unit`, `type` of the value from the data section.


## Metadata extraction and the data model for the central metadata database

As explained above, the data in Minio is organised in files based on a device and date. For every such file, metadata is extracted and stored in the **central metadata database** (PostgreSQL). This database uses the following data model that is designed to conveniently locate data files holding the information related to particular projects, devices or quantities. The central metadata database also defines which InfluxDB time series are publicly available.

```
                   +---------------------+
                   | Metadata            |
                   |                     |
                   | uid                 |
                   | project_id          |
                   | device_id           |
                   | description         |
                   | type                |
                   | manufacturer        |
                   | serial              |
                   | placement_timestamp |
                   | location            |
                   | device_uid          |
                   +---------------------+
                              N
                              |
                              1
+----------------+     +-------------+     +------------+
| Project        |     | Device      |     | File      |
|                |     |             |     |            |
| uid            | 1-N | uid         | 1-N | uid        |
| project_id     |     | project_id  |     | date       |
| name           |     | device_id   |     | project_id |
| description    |     | project_uid |     | device_id  |
| contact_person |     +-------------+     | messages   |
| email          |            M            | filename   |
| link           |            |            +------------+
+----------------+            N
                       +-------------+
                       | Quantity    |
                       |             |
                       | uid         |
                       | name        |
                       | description |
                       | unit        |
                       | type        |
                       +-------------+

+----------------+
| DeviceQuantity |
|                |
| device_uid     |
| quantity_uid   |
+----------------+

+--------------+
| Public       |
|              |
| device_uid   |
| quantity_uid |
+--------------+
```

Tables and relationships:
- `devices`: Every device is uniquely defined by `project_id` and `device_id`.
- `projects`: This table holds addional information about every project. Each device belongs to one project.
- `metadata`: This table stores the unique metadata sent with every message to the platform. Each device can have multiple metadata sections. As explained above, metadata for a given device can be updated throughout its lifetime (e.g. replacement leading to a new serial number, or pacing a device to a new location). It should be pointed out that the location coordinates are removed from metadata in the database.
- `files`: Data from every device is organised in files based on the calendar date. The filename and the number of messages it contains is available here.
- `quantities`: This table shows what quantities are available in the plaform. The fields from the value of the data section sent in the messages, except for the value itself, are available here. Each device can measure multiple quantities. The same quantity can be measured by multiple devices.
- `device_quantity`: This table holds the indices for the many-to-many relationship between the `devices` and `quantities` tables.
- `public`: This table holds the indices for the many-to-many relationship between the `devices` and `quantities` tables for the time series that are publicly available.


## Data formats in Minio

- The `kafka-connect` bucket is used for storing the batches of messages coming from Kafka. The messages in the generic schema format are serialized in [Avro](https://avro.apache.org/) files.
- The data files can be re-formatted into CSV files on demand for convenience reasons. The CSV files give flat tables that are easier to analyse than the embedded JSON of the generic schema. These CSV files start with a header that list the unique metadata and unique quantities available in the file. The same directory structure and file naming conventions as above are followed, except for the `.csv` file extension. An example CSV file is shown below.

Example CSV file:

```
# Metadata
# {"description": "test device", "device_id": "device1", "location": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": null, "type": null}
# {"description": "test device", "device_id": "device1", "location": null, "manufacturer": null, "placement_timestamp": null, "project_id": "test", "serial": "1234", "type": null}
# Quantities
# {"description": "temperature", "name": "Temperature", "type": "INT", "uid": 1, "unit": "degrees"}
# {"description": "humidity", "name": "Relative humidity", "type": "FLOAT", "uid": 2, "unit": ""}
,,,,temperature,humidity
,,,,Temperature,Relative humidity
,,,,degrees,
,,,,INT,FLOAT
timestamp,time,project_id,device_id,1,2
0,1970-01-01 00:00:00.000000,test,device1,42.0,
1,1970-01-01 00:00:00.001000,test,device1,,0.0
2,1970-01-01 00:00:00.002000,test,device1,,1.0
```
