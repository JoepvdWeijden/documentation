# Making time series data public
Time series data from InfluxDB can be made public, exposing it on the public HTTP API. This API is exposed to the internet and is queryable by anyone without authentication. By exposing a time series, data from the last seven days will be made available, aggregated on an hourly basis. More information about the public HTTP API can be found on the [separate documentation page](../PUBLIC-API.md).

## Making data public
Data can be made public using the [cluster management API](https://flask.green-village.sda-projects.nl/).

List all available time series for project A:

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/public/A" \
  -u admin:password \
  -H "accept: application/json"
```

The API call above will return a list of device UID - quantity UID combinations, each uniquely identifying a time series.

Time series can be made public by passing a list of device UID - quantity UID pairs. The time series identified by these pairs will be made publicly available. In case an empty list is passed, all time series belonging to the project will be made public. The call will result in an error in case the list contains a pair that does not identify a time series belonging to the project.

The following example shows how to make the time series for device UID `1` and quantity UID `2` public:

```sh
$ curl -X POST \
  "https://flask.green-village.sda-projects.nl/public/A" \
  -u admin:password \
  -H "accept: application/json" \
  -H "Content-Type: application/json" \
  --data-urlencode '[{"device_uid": 1, "quantity_uid": 2}]'
```

List all time series for project A again to verify the time series with device UID `1` and quanity UID `2` is made public (this is exactly the same call as the first call above):

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/public/A" \
  -u admin:password \
  -H "accept: application/json"
```

To make a time series with device UID `1` and quantity UID `2` private again, call the API below. In case an empty list is passed, all time series belonging to the project will be made private. The call will result in an error in case the list contains a pair that does not identify a time series belonging to the project.

```sh
$ curl -X DELETE \
  "https://flask.green-village.sda-projects.nl/public/A" \
  -u admin:password \
  -H "accept: application/json" \
  -H "Content-Type: application/json" \
  --data-urlencode '[{"device_uid": 1, "quantity_uid": 2}]'
```

Verify that this time series is made private by listing all time series for project A:

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/public/A" \
  -u admin:password \
  -H "accept: application/json"
```
