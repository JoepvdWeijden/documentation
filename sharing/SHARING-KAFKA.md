# Sharing data streams from Kafka
Each project has a topic defined in Kafka that producers can send data to, as described in the [usage information](../USAGE.md#kafka). The topic is named `PROJECT_NAME-schema`.

The topic can be shared, allowing project users to consume topics from other projects. This access is **read-only**: only consumption is allowed, not production.

## Sharing a project's topic
To allow the project user from project B to consume from the topic of project A (topic `A-schema`), we use the following API call:

```sh
$ curl -X POST \
  "https://flask.green-village.sda-projects.nl/kafka/A-schema/share/B" \
  -u admin:password \
  -H "accept: application/json"
```

Verify the project user from project B can read from topic `A-schema`:

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/kafka/A-schema" \
  -u admin:password \
  -H "accept: application/json"
```

Project user B can now read from topic `A-schema`, using one of the methods shown in the [examples](https://gitlab.com/the-green-village/examples).

Revoke access from project B:

```sh
$ curl -X DELETE \
  "https://flask.green-village.sda-projects.nl/kafka/A-schema/share/B" \
  -u admin:password \
  -H "accept: application/json"
``
