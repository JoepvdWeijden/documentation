# Sharing time series data from InfluxDB
Each project stores time series data in its own, isolated InfluxDB instance. **Read-only** access can be provided to another project. This access is all-or-nothing: either all data for all sensors is shared, or no data at all.

## Sharing data
Share all data from project A with project B as follows:

```sh
$ curl -X POST \
  "https://flask.green-village.sda-projects.nl/influxdb/A/share/B" \
  -u admin \
  -H "accept: application/json"
```

Verify sharing is enabled:

```sh
$ curl -X GET \
  "https://flask.green-village.sda-projects.nl/influxdb/A" \
  -u admin \
  -H "accept: application/json"
```

The project user from project B can now access project A's InfluxDB's instance as detailed in the [usage information](USAGE.md#influxdb).

Disable sharing from project A to project B:

```sh
$ curl -X DELETE \
  "https://flask.green-village.sda-projects.nl/influxdb/A/share/B" \
  -u admin \
  -H "accept: application/json"
```
